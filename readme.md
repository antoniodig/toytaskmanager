
Requirements: java 1.8+.

To execute the tests:

    cd toytaskmanager
    $ ./gradlew test


# sample usage

Configure the taskmanager in `taskmanager.properties`

```
# allowed values are NONE, FIFO, PRIORITY
processList.evictionPolicy=NONE
processList.capacity=1000000

```

Get the singleton instance of the `TaskManager`, add processes, kill processes...

```java
TaskManager tm = TaskManager.getInstance();

tm.add(new Process(1, MEDIUM));
tm.add(new Process(7, LOW));
tm.add(new Process(5, HIGH));

tm.kill(7));
tm.killGroup(LOW);

List<Process> l = tm.listBy(AGE);
// > returns processes 1, 5

tm.killAll());
// no process left running
```



# assumptions

* All operations on the task manager may be invoked concurrently by clients. This is modeled by a single task manager being able to serve multiple threads at one time.
* The `kill()` operation is less frequent than adding new or querying existing processes. In our implementation, killing a specific process may cost (from the point of view of the task manager) the traversal of the whole list of processes.
* In the default implementation, the way the task manager communicates the failure to a client attempting to add a new process, is by raising an exception. In the case of the Priority-based implementation it just skips it silently (as an alternative, `TaskManager.add(process)` could have just returned a boolean in all cases).
* The ID can be modeled using a `int` primitive type, i.e. the cardinality of `int` is large enough to cater for all running processes at any given moment (on my machine the hard limit for the max processes is 4M+, so in this case an `int` is more than enough).

# design

One could have probably coded a simpler implementation of the methods `add()` and `killX()` of the `TaskManager`, synchronizing all operations on the backing `Queue`. Likely, the cost of the contention on every single operation would be negligible in terms of the cost, paid by the OS, to start or kill the relevant processes.  
However, supposing that a goal of the exercise is finding a relatively cheap (CPU-wise) concurrency management mechanism, the proposed solution is based on:

* using a standard thread-safe nonblocking `Queue` (`ConcurrentLinkedQueue`).
* and coordinating the residual capacity of that backing `Queue` on an atomic counter which at all moment keeps track of the size of the `Queue`. This is expected to be cheaper than synchronizing each method on the `Queue` itself (which also, would entail exclusive access to the `Queue` until the full execution of a given method is completed). Note that the built-in `ConcurrentLinkedQueue.size()` method would be both slow and unapt to limit the size of the queue in a concurrent scenario.

The `TaskManager` is a facade, a thin singleton that wraps the actual manager of the list of running processes, which is an object of type `RunningProcessesListI`.

`RunningProcessesListI` is an interface, whose implementation `RunningProcessesListImpl` offers three default "eviction" strategies, which differ on how they handle the addition of new processes when the capacity is full. (on top of that, `RunningProcessesListImpl` can be extended by injecting custom strategies, which are objects of type `RunningProcessesListImpl.EvictionStrategy`).

All operations on `RunningProcessesListImpl` are lock-free, with one exception. When the capacity is full, the FIFO and Priority-based implementations degrade their parallelism: if different threads are trying to `add()` new processes concurrently, FIFO and Priority-based serialise the deletion of the old process and the subsequent addition of the new one, so they are processed sequentially by the contending threads.


