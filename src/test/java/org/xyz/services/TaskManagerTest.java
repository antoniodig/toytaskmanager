package org.xyz.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.xyz.services.TaskManager.ListProcessesBy.AGE;
import static org.xyz.services.TaskManager.ListProcessesBy.ID;
import static org.xyz.services.TaskManager.ListProcessesBy.PRIORITY;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.xyz.entities.Process;
import org.xyz.entities.Process.Priority;
import org.xyz.providers.UnableToRunNewProcessException;
import org.xyz.providers.RunningProcessesListI;
import org.xyz.providersImpl.RunningProcessesListImpl;
import org.xyz.providersImpl.RunningProcessesListImpl.EvictionStrategyName;
import org.xyz.services.TaskManager;

@DisplayName("TaskManager - add, kill, list processes")
public class TaskManagerTest {
    
    TaskManager tmNONE;
    RunningProcessesListI rpNONE;
    final int CAPACITY = 10;
    
    //initial (logical) content of the list, before applying the methods under test
    List<Process> originalListOfProcesses;
    
    //Expected results, after a method was applied
    List<Process> expectedListOfProcesses;
    
    @BeforeEach
    void setup() throws UnableToRunNewProcessException {
        
        rpNONE = new RunningProcessesListImpl(CAPACITY, EvictionStrategyName.NONE); 
        tmNONE = new TaskManager(rpNONE);
        
        Priority priority[] = Priority.values();
    
        
        //CAPACITY is 10.
        //Now, create 9 processes with sequential IDs, and cycling priority like these (starts at MEDIUM):
        //1 id:0, priority:MEDIUM
        //2 id:10, priority:HIGH
        //3 id:20, priority:LOW
        //4 id:30, priority:MEDIUM
        //5 id:40, priority:HIGH
        //6 id:50, priority:LOW
        //7 id:60, priority:MEDIUM
        //8 id:70, priority:HIGH
        //9 id:80, priority:LOW
        originalListOfProcesses = new ArrayList<Process>(CAPACITY);
        
        for(int i = 0; i < CAPACITY-1; i++) {
            originalListOfProcesses.add(
                    Mockito.spy(
                            new Process(i*10, priority[(i+1) % priority.length])
                            )
                    );
        }

        expectedListOfProcesses = new ArrayList<Process>(CAPACITY);
        expectedListOfProcesses.addAll(originalListOfProcesses); //starts with a clone of the list
        
        //invoke the taskmanager to add all the Processes
        for(Process p : originalListOfProcesses) {
            tmNONE.add(p);
        }
    }
    
    //On each operation, we check that the size() is equal
    //to the length of the expected processes
    //Given the importance of the internal counter, we are essentially
    //checking that it is kept up to date
    private void assertCounterCountsOk(RunningProcessesListI rp, List<Process> expectedListOfProcesses) {
        assertEquals(rp.size(), expectedListOfProcesses.size(),
                "internal atomic counter and length of list of processes must be the same");
    }
    


    @Test
    @DisplayName("GIVEN the default taskManager, "
            + "WHEN adding a Process beyond the max capacity, "
            + "THEN should throw an exception")
    public void whenOverflowingDefaultShouldRaiseException() {
        
        Process p = new Process(CAPACITY-1, Priority.LOW);
        
        assertThrows(UnableToRunNewProcessException.class, () ->
        {
            tmNONE.add(p);
            tmNONE.add(new Process(CAPACITY, null));
        });
        
        expectedListOfProcesses.add(p);
        assertIterableEquals(tmNONE.listBy(AGE), expectedListOfProcesses,
                "new list should be previous + just one added Process");
        
        assertCounterCountsOk(rpNONE, expectedListOfProcesses);
        
    }

    @Test
    @DisplayName("GIVEN the FIFO taskManager, "
            + "WHEN adding a Process beyond the max capacity, "
            + "THEN should add the new Process, "
            + "AND should evict the oldest")
    public void whenOverflowingFifoShouldEvictOldest() throws UnableToRunNewProcessException {
        
        //Warning, we are ignoring the common setup RunningProcessListImpl
        RunningProcessesListI rpFIFO
            = new RunningProcessesListImpl(CAPACITY, EvictionStrategyName.FIFO);
        TaskManager fifoTaskMgr = new TaskManager(rpFIFO);
        
        //invoke the taskmanager to take in all the Processes
        for(Process p : originalListOfProcesses) {
            fifoTaskMgr.add(p);
        }
        
        Process p1 = new Process(CAPACITY-1, Priority.LOW);
        Process p2 = new Process(CAPACITY, Priority.LOW);
        

        //this is ok
        fifoTaskMgr.add(p1);
        //this is added as well, but replacing the oldest
        fifoTaskMgr.add(p2);
        
        Process expectEvicted = expectedListOfProcesses.get(0);
        //add the newest
        expectedListOfProcesses.add(p1);
        expectedListOfProcesses.add(p2);
        //remove the first
        expectedListOfProcesses.remove(0);

        assertIterableEquals(fifoTaskMgr.listBy(AGE), expectedListOfProcesses,
                "new list should be previous + just one added Process - minus oldest process");
        //process 3 was killed
        verify(expectEvicted).kill();
        
        assertCounterCountsOk(rpFIFO, expectedListOfProcesses);
        
    }
    
    @Test
    @DisplayName("GIVEN the PRIORITY-based taskManager, "
            + "WHEN adding a MEDIUM-priority Process beyond the max capacity, "
            + "THEN should add the new Process, "
            + "AND should evict the oldest LOW-Priority one")
    public void whenOverflowingPriorityShouldReplaceOldestLowestPriority() throws UnableToRunNewProcessException {
        
        //Warning, we are ignoring the common setup RunningProcessListImpl
 
        RunningProcessesListI rpPRIORITY
            = new RunningProcessesListImpl(CAPACITY, EvictionStrategyName.PRIORITY);
        TaskManager priorityTaskMgr = new TaskManager(rpPRIORITY);
        
        //invoke the taskmanager to take in all the Processes
        for(Process p : originalListOfProcesses) {
            priorityTaskMgr.add(p);
        }
        
        Process p1 = new Process(CAPACITY-1, Priority.MEDIUM);
        Process p2 = new Process(CAPACITY, Priority.MEDIUM);
        

        //this is ok
        priorityTaskMgr.add(p1);
        //this is added as well, but replacing the oldest
        priorityTaskMgr.add(p2);
        
        //the first process with LOW priority is the third
        Process expectEvicted = expectedListOfProcesses.get(2);
        //add the newest
        expectedListOfProcesses.add(p1);
        expectedListOfProcesses.add(p2);
        //remove the first
        expectedListOfProcesses.remove(2);

        assertIterableEquals(priorityTaskMgr.listBy(AGE), expectedListOfProcesses,
                "new list should be previous + just one added Process - minus oldest lowest-priority process");
        //process 3 was killed
        verify(expectEvicted).kill();
        
        assertCounterCountsOk(rpPRIORITY, expectedListOfProcesses);
        
    }
    
    
    @Test
    @DisplayName("GIVEN the PRIORITY-based taskManager, "
            + "AND a list of tasks all MEDIUM priority, "
            + "WHEN adding a MEDIUM priority Process beyond the max capacity, "
            + "THEN should silently left the list unchanged, AND kill no process")
    public void whenOverflowingPriorityShouldLeaveUnchanged() throws UnableToRunNewProcessException {
        
        //Warning, we are ignoring the common setup RunningProcessListImpl
        
        RunningProcessesListI rpPRIORITY
            = new RunningProcessesListImpl(CAPACITY, EvictionStrategyName.PRIORITY);
        
        TaskManager priorityTaskMgr = new TaskManager(rpPRIORITY);
        

        List<Process> beforeList = new ArrayList<Process>(CAPACITY);
        //add only MEDIUM priority
        for(int i = 0; i < CAPACITY-1; i++) {
            beforeList.add(Mockito.spy(new Process(i*10, Priority.MEDIUM)));
        }
        
        List<Process> expectedAfterList = new ArrayList<Process>(CAPACITY);
        expectedAfterList.addAll(beforeList); //starts with a clone of the list
        
        for(Process p : beforeList) {
            priorityTaskMgr.add(p);
        }
 
        Process p1 = new Process(CAPACITY-1, Priority.MEDIUM);
        Process p2 = new Process(CAPACITY, Priority.MEDIUM);
        

        //this is ok
        priorityTaskMgr.add(p1);
        //this is not going to be added
        priorityTaskMgr.add(p2);
        
        //so the expected list just has the first process added
        expectedAfterList.add(p1);

        assertIterableEquals(priorityTaskMgr.listBy(AGE), expectedAfterList,
                "new list should be previous + just one added Process");
        
        //also verify that no process was killed 
        beforeList.stream().forEach(p -> verify(p, never()).kill());
        
        assertCounterCountsOk(rpPRIORITY, expectedAfterList);
        
    }

    
    ///////////////////////////////////////////////////////////////////////////
    //From this point on, the behaviour is independent of the eviction strategy.
    //So test everything just once with strategy=NONE
    
    
    @Test
    @DisplayName("GIVEN less running Processes than the max capacity, "
            + "WHEN trying to add a new Process, "
            + "THEN should add it")
    public void shouldAddANewProcess() throws UnableToRunNewProcessException {
        
        Process p = new Process(CAPACITY-1, Priority.LOW);
        tmNONE.add(p);
        expectedListOfProcesses.add(p);
        assertIterableEquals(tmNONE.listBy(AGE), expectedListOfProcesses,
                "new list should be previous + added Process");
        
        assertCounterCountsOk(rpNONE, expectedListOfProcesses);
    }
    
    
    @Test
    @DisplayName("WHEN killing an existing Process, "
            + "THEN it should disappear from the list of running processes, "
            + "AND kill() should have been invoked on the Process")
    public void shouldRemoveSingleProcess() {

        //process 3 is present
        Process p30 = tmNONE.listBy(AGE).stream().filter(p -> p.getId() == 30).findAny().get();
        
        tmNONE.kill(30);
        
        //process 3 has disappeared
        assertEquals(0, tmNONE.listBy(AGE).stream().filter(p -> p.getId() == 30).count());

        //process 3 was killed
        verify(p30).kill();
        
        assertCounterCountsOk(rpNONE, tmNONE.listBy(AGE));
        
    }
    
    @Test
    @DisplayName("WHEN killing a group of Processes by priority, "
            + "THEN they should disappear from the list of running processes, "
            + "AND kill() should have been invoked on all of them")
    public void shouldRemoveProcessByPriority() {
        
        //there are processes with low priority
        assertTrue(tmNONE.listBy(AGE).stream().filter(p -> p.getPriority() == Priority.LOW).count() > 0);
        
        tmNONE.killGroup(Priority.LOW);
        
        //LOW processes have disappeared
        assertEquals(0, tmNONE.listBy(AGE).stream().filter(p -> p.getPriority() == Priority.LOW).count());
        
        //all of them have been killed
        originalListOfProcesses.stream().filter(p -> p.getPriority() == Priority.LOW).forEach(p-> verify(p).kill());
        
        assertCounterCountsOk(rpNONE, tmNONE.listBy(AGE));
        
    }
    
    @Test
    @DisplayName("WHEN killing all Processes, "
            + "THEN they all should disappear from the list of running processes, "
            + "AND kill() should have been invoked on all of them")
    public void shouldRemoveAllProcesses() {
        
        tmNONE.killAll();
        //all processes have disappeared
        assertEquals(0, tmNONE.listBy(AGE).stream().count());
        
        //all of them have been killed
        originalListOfProcesses.stream().forEach(p-> verify(p).kill());
        
        assertCounterCountsOk(rpNONE, tmNONE.listBy(AGE));
        
    }

    
    @Test
    @DisplayName("WHEN listing processes by AGE, THEN they are listed from oldest to newest")
    public void shouldListByAge() {
        
        //expectedListOfProcesses is constructed with increasing IDs, so sort by ID
        expectedListOfProcesses.sort(
                (p1, p2) -> {
                    if(p1.getId() == p2.getId()) {return 0;}
                    return p1.getId()>p2.getId() ? 1 :-1;
                });

        assertIterableEquals(tmNONE.listBy(AGE), expectedListOfProcesses);
        
    }
    
    @Test
    @DisplayName("WHEN listing processes by ID, THEN they are listed from first to last ID")
    public void shouldListByID() throws UnableToRunNewProcessException {
        
        
        //add a process which breaks the monotonic sequence of IDs
        //So IDs are not in the same order as age
        Process p = new Process(15, Priority.HIGH);
        tmNONE.add(p);
        expectedListOfProcesses.add(p);
        
        expectedListOfProcesses.sort(
                (p1, p2) -> {
                    if(p1.getId() == p2.getId()) {return 0;}
                    return p1.getId()>p2.getId() ? 1 :-1;
                });

        assertIterableEquals(tmNONE.listBy(ID), expectedListOfProcesses);
        
    }
    
    @Test
    @DisplayName("WHEN listing processes by PRIORITY, THEN they are listed from lowest to highest priority")
    public void shouldListByPriority() {
        
        
        expectedListOfProcesses.sort(
                (p1, p2) -> {
                    if(p1.getPriority().val == p2.getPriority().val) {return 0;}
                    return p1.getPriority().val>p2.getPriority().val ? 1 :-1;
                });

        assertIterableEquals(tmNONE.listBy(PRIORITY), expectedListOfProcesses);
        
    }
    
    //the instantiation is not really responsibility of the taskManager, but of the DI framework.
    //However just quickly sanity-check that the underlying configuration is fine
    @Test
    @DisplayName("The TaskManager Singleton is initialized")
    public void shouldInitialiseTaskManager() {
        assertNotNull(TaskManager.getInstance(), "the taskManager should be initialised as a singleton");
    }
    

}
