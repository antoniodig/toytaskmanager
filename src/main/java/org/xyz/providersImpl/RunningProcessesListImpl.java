package org.xyz.providersImpl;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.xyz.entities.Process;
import org.xyz.entities.Process.Priority;
import org.xyz.providers.RunningProcessesListI;
import org.xyz.providers.UnableToRunNewProcessException;

/**
 * A thread-safe, concurrent implementation of {@link RunningProcessesListI} that manages the list of running processes.
 * <br>When the capacity of the list has been reached, and an attempt is made to add a new process, behaves accordingly to the specified {@link EvictionStrategy}.   
 *
 */
public class RunningProcessesListImpl implements RunningProcessesListI{
    
    /**
     * The counter that keeps track of the number of processes being run
     */
    private AtomicInteger runningProcessesCount = new AtomicInteger(0);

    /**
     * the max number of running processes
     */
    private final Integer capacity;
    
    private Queue<Process> runningProcesses = new ConcurrentLinkedQueue<Process>();
    private final EvictionStrategy evictionStrategy;
     
   
  
    /**
     * Returns an implementation of a {@link RunningProcessesListI} with the given capacity and the specified
     * eviction policy
     * @param capacity the max number of processes that can be run at the same time
     * @param evictionStrategy a custom {@link EvictionStrategy} to use
     */
    public RunningProcessesListImpl(Integer capacity, EvictionStrategy evictionStrategy) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("the max capacity should be > 0, it was " + capacity + " instead");
        }
        this.capacity = capacity;
        this.evictionStrategy = evictionStrategy;
        
    }
    
    /**
     * Returns an implementation of a {@link RunningProcessesListI} with the given capacity and the specified
     * built-in eviction policy
     * @param capacity the max number of processes that can be run at the same time
     * @param evictionStrategyName the {@link EvictionStrategyName} to use
     */
    
    public RunningProcessesListImpl(Integer capacity, EvictionStrategyName evictionStrategyName) {
        
        if (capacity <= 0) {
            throw new IllegalArgumentException("the max capacity should be > 0, it was " + capacity + " instead");
        }
        this.capacity = capacity;
        
        
        switch(evictionStrategyName) {
        case NONE:
            this.evictionStrategy = new EvictionStrategyNone();
          break;
        case FIFO:
            this.evictionStrategy = new EvictionStrategyFifo();
          break;
        case PRIORITY:
            this.evictionStrategy = new EvictionStrategyPriority();
          break;
        default:
          throw new InvalidParameterException("evictionStrategy '" + evictionStrategyName + "' is not implemented");
      }
    }
    
    
    
    /**
     * Returns the default implementation of a {@link RunningProcessesListI} using {@link EvictionStrategyName#NONE}
     * @param capacity the max number of processes that can be run at the same time
     */
    
    public RunningProcessesListImpl(Integer capacity) {
        this(capacity, EvictionStrategyName.NONE);
    }
    


    
    
    @Override
    public void add(Process process) throws UnableToRunNewProcessException {
        
        //if the new process can be added without exceeding
        //the max number of running processes, increment the count of running processes,
        // and add the process,
        if(runningProcessesCount.incrementAndGet() <= capacity) {
            runningProcesses.add(process);
        }
        //otherwise we should kill other processes (or do nothing if eviction = NONE)
        //and in most cases decrease the counter again.
        else {
            try {
                int deltaProcesses = evictionStrategy.apply(process, runningProcesses);
                //Change the number of processes as indicated by deltaProcesses. 
                //But also, at the beginning of this method we had incremented the number of processes by 1,
                //so compensate for that with -1 here
                runningProcessesCount.addAndGet(deltaProcesses-1);
            }
            catch(UnableToRunNewProcessException e){
                //if we are here, it means that the new process was not added,
                //so "restore" the count to the previous value 
                runningProcessesCount.decrementAndGet();
                //Re-throw, to signal the client that the given eviction strategy has
                //explicitly thrown the exception instead of implicitly skipping the new process
                throw e;
            }
                        
        }
        
    }

    @Override
    public void kill(int id) {

        //Scan the list of processes to find the one with the given id
        //This operation costs O(n), where n is the length of the list
        
        //We have to find the Process p before killing it, because we have to
        // invoke p.kill()
        //Adding (and maintaining) a Hashmap of (id, p) would have made this lookup
        //quicker, but also the code a bit more complex.
        runningProcesses.stream()
                .filter(p -> p.getId() == id)
                .findAny()
                .ifPresent(p -> removeFromListAndKill(p));
    }

    @Override
    public void killGroup(Priority pr) {
        //get a photo of all Processes with the given priority
        runningProcesses.stream()
                .filter(p -> p.getPriority() == pr)
                .forEach(p -> {removeFromListAndKill(p);}
                        
            );
        
        
    }

    @Override
    public void killAll() {
        
        Process p = runningProcesses.poll();
        
        //pop all elements, one by one, and kill them
        while(p != null) {
            
            p.kill();
            runningProcessesCount.decrementAndGet();
            
            p = runningProcesses.poll();
        }
        
    }



    private boolean removeFromListAndKill(Process p) {
        boolean wasRemoved = false;
        //atomically removes the process: no two threads can remove it twice
        wasRemoved = runningProcesses.remove(p);
        if(wasRemoved) {
            //if it was removed, kill it
            p.kill();
            runningProcessesCount.decrementAndGet();
        }
        return wasRemoved;
    }

    


    @Override
    public Stream<Process> streamOrderByAge() {
        return runningProcesses.stream();
    }



    @Override
    public int size() {
        return runningProcessesCount.get();
    }



    @Override
    public int maxCapacity() {
        return capacity;
    }
        
        
    /** 
     * an interface to let inject a custom eviction policy.
     *
     */
    @FunctionalInterface
    public interface EvictionStrategy{
        
        /**
         * Invoked when the client asks to {@link #add} a new Process
         * but the list of running processes is full
         * <br>
         * The implementer, depending on the strategy,
         * may add or not the new process to the list. 
         * It figures out whether to remove and what process to remove,
         * and removes it from the list of processes.
         * <br>Also invokes {@link Process#kill} on the removed process (if any).
         * <br>The counter, is decreased by one if it has evicted a process, and also decreased by one
         * again decreased by one if it has not added any process.
         * 
         * @param process the new process being added
         * @param runningProcesses the list of currently
         * running processes, ordered from oldest to newest
         * @return the overall number of processes added minus the number of processes that has removed. If no process was
         * added nor evicted, returns 0 or throws an {@link UnableToRunNewProcessException},
         * depending on the implemented policy 
         * @throws UnableToRunNewProcessException 
         */
        public int apply(
                Process process,
                Queue<Process> runningProcesses
        ) throws UnableToRunNewProcessException;
    }



    /**
     * 
     * Refers to the built-in policies for when and how older processes must be killed, in case {@link RunningProcessesListImpl#add} is invoked
     * but the list of processes has already reached its full capacity.
     * <br>The provided policies are:
     * <ul>
     * <li>{@link #NONE}: doesn't kill older processes and throws {@link UnableToRunNewProcessException}</li>
     * <li>{@link #FIFO}: kills the oldest process</li>
     * <li>{@link #PRIORITY}: kills the oldest process with lowest priority. However if no process is found having priority
     * lower than the one being added, then {@link add} method silently does nothing (the addition is skipped)</li>
     * </ul>
     *
     */
    public static enum EvictionStrategyName {
        NONE
        ,FIFO
        ,PRIORITY;
    }
    

    private static class EvictionStrategyNone implements EvictionStrategy{

        @Override
        public int apply(Process process, Queue<Process> runningProcesses) throws UnableToRunNewProcessException {
            //Don't add the new process, just complain...
            throw new UnableToRunNewProcessException(
                    String.format("Cannot run the new process with id [%d]: OS has reached max number of processes%n", process.getId())
                );
        }
        
    }
    
    //swaps old processes for new ones
    private static class EvictionStrategyFifo implements EvictionStrategy{

        @Override
        public int apply(Process process, Queue<Process> runningProcesses) {
            Process evictedProcess; 

            //Serialise the operation of swapping an old process for the new one.
            // It is to protect from an excessive number of threads (more than the capacity)
            // trying to add processes simultaneously, which would end up with more running processes
            // than the capacity
            
            int addedProcessesCount = 0; 
            
            synchronized(runningProcesses) {
                //remove the oldest process
                evictedProcess = runningProcesses.poll();
                
                //if no process was popped, it means that others threads have killed
                //one or more, so the list is empty now.
                if(evictedProcess != null) {
                    evictedProcess.kill();
                    addedProcessesCount--;
                }
                
                //in any case, at this point, we can safely add the new process
                runningProcesses.add(process);
                addedProcessesCount++;
            
            }
            return addedProcessesCount; 
            
        }
    }
    
    /**
     * Removes the oldest process with lowest priority, if it is lower than the newly added process.
     * "Oldest" means the first process from this ordering: "order by PRIORITY asc, AGE asc"
     */
    private static class EvictionStrategyPriority implements EvictionStrategy{


        @Override
        public int apply(Process process, Queue<Process> runningProcesses) {
            
            //if process has already lowest priority, there is no lower-priority process than that, so give up...
            if(process.getPriority() == Priority.lowest()) {
                //no process was added
                return 0;
            }
            
            int newProcessesCount = 0;
            Process evictedProcess = null;
            
            //Serialise the operation of swapping an old process for the new one.
            // It is to protect from an excessive number of threads (more than the capacity)
            // trying to add processes simultaneously, which would end up with more running processes
            // than the capacity, or with many new processes evicting the same old process at the same time
            
            synchronized(runningProcesses) {
                
                //get the minimum priority, which is less than the new process priority
                
                //in a single scan, for each priority (less than the new process')
                //get the first process in the list (which is the oldest)
                
                Map<Priority, Process> oldestByPriority = runningProcesses.stream()
                        //priority must be lower than this process' priority
                        .filter(p -> p.getPriority().val < process.getPriority().val)
                        //collect into a map like {LOW: 'Process 21', MEDIUM: 'Process 128'}
                        .collect(
                                Collectors.toMap(Process::getPriority,
                                        v -> v, //identity
                                        (v1, v2) -> v1) //preserve the first element with the same priority
                        );
                                
                //from lowest to highest priority, get the (oldest) process for each priority
                //and keep the first (i.e. the lowest priority one)
                for(Priority priority : Priority.values()) {
                    evictedProcess = oldestByPriority.get(priority);
                    if(evictedProcess != null) {
                        break;
                    }
                }
                

                //finally, if there is such process, remove it
                if(evictedProcess != null) {
                    // if no process was removed, it means that others threads have killed it in
                    // the meanwhile, which is fine anyway
                    if(runningProcesses.remove(evictedProcess)) {
                        evictedProcess.kill();
                        //a process was evicted, so decrement the counter
                        newProcessesCount--;
                    }
                
                
                    //in any case, at this point, we can safely add the new process
                    runningProcesses.add(process);
                    newProcessesCount++;
                }
                
            }
            
            return newProcessesCount; 
        }
    }
    
    
    
}

