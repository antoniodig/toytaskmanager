package org.xyz.services;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.xyz.entities.Process;
import org.xyz.entities.Process.Priority;
import org.xyz.providers.RunningProcessesListI;
import org.xyz.providers.UnableToRunNewProcessException;
import org.xyz.providersImpl.RunningProcessesListImpl;
import org.xyz.providersImpl.RunningProcessesListImpl.EvictionStrategyName;

public class TaskManager {
    
    private RunningProcessesListI runningProcessesList;

    
    /**
     * Specifies a sort order when listing the
     * running processes via the {@link #listBy} method
     *
     */
    public enum ListProcessesBy {AGE, ID, PRIORITY}
    
    TaskManager(RunningProcessesListI processListImplInstance) {
        this.runningProcessesList = processListImplInstance;
    }

    /** 
     * get the singleton instance
     * @return
     */
    public static TaskManager getInstance() {
        return TaskManagerInitialiser.tm;
    }
    
    /** 
     * add a Process. Depending on the concrete implementation could throw an exception when
     * the capacity is full 
     * @param p
     * @throws UnableToRunNewProcessException
     */
    public void add(Process p) throws UnableToRunNewProcessException{
        runningProcessesList.add(p);
    }


    /** lists the running processes
     * 
     * @param order enum of type {@link ListProcessesBy}
     * @return a {@link List} of {@link Process Processes}
     */
    public List<Process> listBy(ListProcessesBy order) {
        Stream<Process> streamByAge = runningProcessesList.streamOrderByAge();
        Stream<Process> retStream = streamByAge;
        switch (order) {
        case AGE:
            //sort by AGE
            retStream = streamByAge;
            break;
        case ID:
            //sort by ID
            retStream = streamByAge.sorted((p1,p2) -> {
                if(p1.getId() > p2.getId()) {return 1;}
                if(p1.getId() < p2.getId()) {return -1;}
                return 0;
            });
            break;
        case PRIORITY:
            //sort by PRIORITY
            retStream = streamByAge.sorted((p1,p2) -> {
                if(p1.getPriority().val > p2.getPriority().val) {return 1;}
                if(p1.getPriority().val < p2.getPriority().val) {return -1;}
                return 0;
            });
            break;
        }
    
        return retStream.collect(Collectors.toList());
    }
    
    /**
     * kill the given process
     * @param id
     */
    public void kill(int id) {
        runningProcessesList.kill(id);
    }
    
    /**
     * kill a group of processes identified by their {@link Process#Priority}
     * @param pr
     */
    public void killGroup(Priority pr) {
        runningProcessesList.killGroup(pr);
    }
    
    /**
     * kills all running processes
     */
    public void killAll(){
        runningProcessesList.killAll();
    }

    
    // Normally this would be done by the DI framework 
    private static class TaskManagerInitialiser {
        public static TaskManager tm;
        
        private static final String CONFIG_FILE = "taskmanager.properties";
        private static final String CONFIG_CAPACITY = "processList.capacity";
        private static final String CONFIG_EVICTION_POLICY = "processList.evictionPolicy";
        
        private static final String DEFAULT_CAPACITY = "1000000";
        private static final String DEFAULT_EVICTION_POLICY_NAME = "NONE";
        
        //  initialise the TaskManager according to the config.properties file.
        //  for example:
        //    
        //      processList.evictionPolicy=NONE
        //      processList.capacity=10
        //    
        static {
            Properties prop = new Properties();;
            int processListCapacity = 0;
            EvictionStrategyName evictionStrategyName;
          
            try {
                //load the configuration from file
                prop.load(TaskManager.class.getClassLoader().getResourceAsStream(CONFIG_FILE));
                
                //get the max number of running processes and the name of the EvictionStrategy
                processListCapacity = Integer.parseInt(prop.getProperty(CONFIG_CAPACITY, DEFAULT_CAPACITY));
                evictionStrategyName = EvictionStrategyName.valueOf(prop.getProperty(CONFIG_EVICTION_POLICY, DEFAULT_EVICTION_POLICY_NAME));
            }catch(IOException  e) {
                throw new IllegalStateException("unable to read file '" + CONFIG_FILE+ "' from classpath", e);
            }
            
            tm = new TaskManager(new RunningProcessesListImpl(processListCapacity, evictionStrategyName));
         
        }
    }
    
}


