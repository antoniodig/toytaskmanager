package org.xyz.providers;

import java.util.stream.Stream;

import org.xyz.entities.Process;
import org.xyz.entities.Process.Priority;

/**
 * Represents the class that, actually, manages the list of running processes 
 *
 */
public interface RunningProcessesListI {
    
    /**
     * Adds the new process if the max number of processes (capacity) has not
     * been reached. Otherwise specific policies apply.
     * <br> Depending on the policy, if the new process is not added may throw a {@UnableToRunNewProcessException}
     * @param process the process to add
     * @throws UnableToRunNewProcessException
     */
    void add(Process process) throws UnableToRunNewProcessException ;
 
    
    /**
     * removes the process from the list of running processes, and invokes {@link Process#kill()} on it 
     * @param id the id of the process
     */
    void kill(int id);

    
    /**
     * removes all processes with the given {@link Priority} from the list of running processes, and invokes {@link Process#kill()} on each
     *  
     * @param pr the {@link Priority}
     */
    void killGroup(Priority pr) ;
    
    /**
     * removes all processes from the list of running processes, and invokes {@link Process#kill()} on each 
     */
    void killAll();

    /**
     * returns the current number of running processes.
     */
    int size();
    
    /**
     * returns the capacity: the max number of running processes
     */
    int maxCapacity();

    /**
     * returns a stream of the running processes, ordered from the first that was added to the latest
     */
    Stream<Process> streamOrderByAge();
    
}
