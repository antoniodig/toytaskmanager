package org.xyz.providers;

public class UnableToRunNewProcessException extends Exception {

    private static final long serialVersionUID = -151974599171723982L;

    public UnableToRunNewProcessException(String errorMessage) {
        super(errorMessage);
    }
    
    public UnableToRunNewProcessException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }
}
