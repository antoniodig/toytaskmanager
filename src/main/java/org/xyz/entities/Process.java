package org.xyz.entities;

/**
 * 
 * A process is an immutable object representing an OS process.
 * <br>It is identified by its {@link #id} and has a {@link Priority}
 *
 */
public class Process {

    /**
     * 
     * Priority of the Process. In some implementation the OS might
     * kill lower priority processes to make room for new, higher priority ones.
     * This enum has the following characteristics:
     * <ul>
     * <li>Priorities are listed from lowest to highest</li>
     * <li>The {@link #val} of each item increases with its logical priority</li>
     * </ul>
     * Values are {@link #LOW}, {@link #MEDIUM}, {@link #HIGH}
     *
     */
    public static enum Priority {
        LOW (10)
        ,MEDIUM (20)
        ,HIGH (30);
        
        public static Priority lowest() {
            return LOW;
        }
        
        public final int val;
        
        private Priority(int v) {
            this.val = v;
        }
        

    }
    


    private final int id;
    private final Priority priority;
    
    
    /**
     * 
     * @param id uniquely identifies a {@link Process}
     * @param priority is the Process' {@link Priority}
     */
    public Process(int id, Priority priority) {
        this.id = id;
        this.priority = (priority == null ? Priority.LOW : priority);
        
    }


    /**
     * Invokes Process' self killing routine.
     */
    public void kill() {
            System.out.format("process [%d] was KILLED%n", this.id);
    }
    
    
    public int getId() {
        return id;
    }


    public Priority getPriority() {
        return priority;
    }

    //just based on the id
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    //just based on the id
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Process other = (Process) obj;
        if (id != other.id)
            return false;
        return true;
    }
    
}
